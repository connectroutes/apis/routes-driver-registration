module routes-driver-registration

go 1.12

require (
	github.com/DataDog/zstd v1.4.4 // indirect
	github.com/go-redis/redis/v7 v7.1.0
	github.com/golang/protobuf v1.4.1
	github.com/google/uuid v1.1.1
	github.com/joho/godotenv v1.3.0
	github.com/nyaruka/phonenumbers v1.0.54
	github.com/pkg/errors v0.9.0 // indirect
	github.com/stretchr/testify v1.4.0 // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	gitlab.com/connectroutes/routes-go-lib v0.0.0-20200929132106-5f189ebc5f19
	go.mongodb.org/mongo-driver v1.3.0
	golang.org/x/crypto v0.0.0-20200311171314-f7b00557c8c4
	golang.org/x/net v0.0.0-20200226121028-0de0cce0169b
	google.golang.org/grpc v1.27.1
	google.golang.org/protobuf v1.25.0
)
