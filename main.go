package main

import (
	"fmt"
	_ "github.com/joho/godotenv/autoload"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"log"
	"net"
	"routes-driver-registration/proto"
)

func main() {
	port := "50056"
	address := "0.0.0.0:" + port

	lis, err := net.Listen("tcp", address)

	if err != nil {
		log.Fatalf("Could not listen on port: %v", err)
	}

	fmt.Println("Server Started ", address)

	server := grpc.NewServer()

	proto.RegisterDriverRegistrationServiceServer(server, &service{})

	reflection.Register(server)

	if err := server.Serve(lis); err != nil {
		log.Fatal(err)
	}
}
