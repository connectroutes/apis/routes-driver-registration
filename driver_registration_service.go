package main

import (
	"context"
	routesModels "gitlab.com/connectroutes/routes-go-lib/models"
	"log"
	"routes-driver-registration/models"
	"routes-driver-registration/proto"
)

type service struct {
}

func (s service) SubmitVehicleDetails(ctx context.Context, request *proto.SubmitVehicleDetailsRequest) (*proto.SubmitVehicleDetailsResponse, error) {
	user, _ := routesModels.GetUserFromContext(ctx)

	_, err := routesModels.CreateDocument(ctx, db, "vehicles", routesModels.Vehicle{
		User:    user.Id,
		Model:   request.Model,
		Make:    request.Make,
		Color:   request.Color,
		Year:    request.Year,
		PlateNo: request.LicensePlate,
	})

	if err != nil {
		return nil, err
	}

	return &proto.SubmitVehicleDetailsResponse{Message: "Vehicle successfully created"}, nil

}

func (s service) GetVehicleModelsOrMakes(ctx context.Context, request *proto.GetVehicleModelsOrMakesRequest) (*proto.GetVehicleModelsOrMakesResponse, error) {
	vehicleNames, err := models.GetVehicleModelsOrMakes(ctx, db, request.GetMake())

	if err != nil {
		log.Printf("Error getting vehicle makes: %v", err)
		return nil, err
	}

	return &proto.GetVehicleModelsOrMakesResponse{
		VehicleNames: vehicleNames,
	}, nil

}

/*func (s service) RegisterDriver(context.Context, *proto.RegisterDriverRequest) (*proto.RegisterDriverResponse, error) {
	panic("implement me")
}*/
