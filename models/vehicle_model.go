package models

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"golang.org/x/net/context"
	"time"
)

type VehicleModel struct {
	Id    primitive.ObjectID `bson:"_id"`
	Year  string             `json:"year"`
	Model string             `json:"model"`
	Make  string             `bson:"make",json:"make"`
}

var collectionName = "vehicle_models"

func GetVehicleModelsOrMakes(ctx context.Context, db *mongo.Database, make string) ([]string, error) {
	var vehicleMakes []string


	sortStage := bson.D{
		{"$sort", bson.D{
			{"_id", 1},
		}},
	}

	var pipeline  mongo.Pipeline

	if make == ""{
		groupStage := bson.D{
			{"$group", bson.D{
				{"_id", "$make"},
			}},
		}

		pipeline = mongo.Pipeline{groupStage, sortStage}
	} else {
		matchStage := bson.D{
			{
				"$match", bson.D{
				{"make", make},
			},
			},
		}

		groupStage := bson.D{
			{"$group", bson.D{
				{"_id", "$model"},
			}},
		}

		pipeline = mongo.Pipeline{matchStage, groupStage, sortStage}
	}

	opts := options.Aggregate().SetMaxTime(2 * time.Second)

	cursor, err := db.Collection(collectionName).Aggregate(context.TODO(),pipeline, opts)

	defer cursor.Close(ctx)

	if err != nil {
		return nil, err
	}

	var results []bson.M

	if err = cursor.All(ctx, &results); err != nil {
		return nil, err
	}



	for _, result := range results{
		vehicleMakes = append(vehicleMakes, result["_id"].(string))
	}

	return vehicleMakes, nil
}